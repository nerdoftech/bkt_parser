package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPush(t *testing.T) {
	assert := assert.New(t)

	expected := []rune{'(', ')'}
	st := &Stack{}
	st.Push('(')
	st.Push(')')
	assert.Equal(expected, st.stack, "Push1 not working")

	expected = append(expected, []rune{'{', '}'}...)
	st.Push('{')
	st.Push('}')
	assert.Equal(expected, st.stack, "Push2 not working")
}

func Test_peek(t *testing.T) {
	assert := assert.New(t)

	st := &Stack{}
	st.Push('(')
	st.Push(')')
	assert.Equal(')', st.peek(), "peek1 not working")

	st.Push('[')
	assert.Equal('[', st.peek(), "peek2 not working")
}

func Test_pop(t *testing.T) {
	assert := assert.New(t)

	st := &Stack{}
	st.Push('(')
	st.Push(')')
	assert.Equal(')', st.pop(), "pop1 did not return last")

	expected := []rune{'('}
	assert.Equal(expected, st.stack, "pop1 did not remove from stack")

	assert.Equal('(', st.pop(), "pop2 did not return last")
	assert.Zero(len(st.stack), "Stack len should be 0")
}

func TestPopAndIsEmpty(t *testing.T) {
	assert := assert.New(t)

	st := &Stack{}

	// Test case "(}"
	st.Push('(')
	res := st.Pop('}')
	assert.False(res, "Pop1 should have been false")
	expected := []rune{'('}
	assert.Equal(expected, st.stack, "Pop1, stack not what it should be")
	assert.False(st.IsEmpty(), "Pop1, IsEmpty should be False")

	// Test case "()"
	res = st.Pop(')')
	assert.True(res, "Pop2 should have been true")
	assert.True(st.IsEmpty(), "Pop2, IsEmpty should be True")

	// Test case "({)"
	st.Push('(')
	st.Push('{')
	res = st.Pop(')')
	assert.False(res, "Pop3 should have been false")
	expected = []rune{'(', '{'}
	assert.Equal(expected, st.stack, "Pop3, stack not what it should be")
	assert.False(st.IsEmpty(), "Pop3, IsEmpty should be False")
}

func TestIsValid(t *testing.T) {
	assert := assert.New(t)

	res := IsValid("(){}")
	assert.True(res, "test1 should have been valid")

	res = IsValid("({})")
	assert.True(res, "test2 should have been valid")

	res = IsValid("([]){}")
	assert.True(res, "test3 should have been valid")

	res = IsValid("(}")
	assert.False(res, "test4 should have not been valid")

	res = IsValid("([)]")
	assert.False(res, "test5 should have not been valid")
}
