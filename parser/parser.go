package parser

type Stack struct {
	stack []rune
}

func (s *Stack) Push(c rune) {
	s.stack = append(s.stack, c)
}

func (s *Stack) peek() rune {
	return s.stack[len(s.stack)-1]
}

func (s *Stack) pop() rune {
	last := s.peek()
	s.stack = s.stack[:len(s.stack)-1]
	return last
}

// Returns true if input was valid closing char
func (s *Stack) Pop(c rune) bool {
	last := s.peek()
	switch last {
	case '(':
		if c == ')' {
			s.pop()
			return true
		}
	case '[', '{':
		// [ = 91, ] = 93; { = 123, } = 125
		if last+2 == c {
			s.pop()
			return true
		}
	}
	return false
}

func (s Stack) IsEmpty() bool {
	return len(s.stack) < 1
}

func IsValid(s string) bool {
	st := &Stack{}
	for _, c := range s {
		switch c {
		case '(', '[', '{':
			st.Push(c)
		case ')', ']', '}':
			res := st.Pop(c)
			// If one Pop fails we know it's not valid
			if !res {
				return false
			}
		}
	}
	return st.IsEmpty()
}
